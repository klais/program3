﻿namespace Program3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea13 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend13 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea14 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend14 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea15 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend15 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Signal_Char = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Sobstv_fun = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Sobstv_znach = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Signal_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.F2 = new System.Windows.Forms.TextBox();
            this.F1 = new System.Windows.Forms.TextBox();
            this.P3 = new System.Windows.Forms.TextBox();
            this.P1 = new System.Windows.Forms.TextBox();
            this.F3 = new System.Windows.Forms.TextBox();
            this.P2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.A1 = new System.Windows.Forms.TextBox();
            this.A2 = new System.Windows.Forms.TextBox();
            this.A3 = new System.Windows.Forms.TextBox();
            this.Signal = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.Array_sig = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Poriadok = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Array_Otsch = new System.Windows.Forms.TextBox();
            this.Text_num_of_sob_vector = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Poligarm_zat = new System.Windows.Forms.RadioButton();
            this.Gauss = new System.Windows.Forms.RadioButton();
            this.Poligarm = new System.Windows.Forms.RadioButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.CHAST_check = new System.Windows.Forms.CheckBox();
            this.N_text = new System.Windows.Forms.TextBox();
            this.M_text = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.NEVYAZKI_TEXTBOX = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.RESULT_BUTTON = new System.Windows.Forms.Button();
            this.RESULT = new System.Windows.Forms.TextBox();
            this.Signal_Char.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Sobstv_fun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sobstv_znach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Signal_chart)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Signal_Char
            // 
            this.Signal_Char.Controls.Add(this.tabPage1);
            this.Signal_Char.Controls.Add(this.tabPage2);
            this.Signal_Char.Location = new System.Drawing.Point(27, 24);
            this.Signal_Char.Name = "Signal_Char";
            this.Signal_Char.SelectedIndex = 0;
            this.Signal_Char.Size = new System.Drawing.Size(1078, 570);
            this.Signal_Char.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.Sobstv_fun);
            this.tabPage1.Controls.Add(this.Sobstv_znach);
            this.tabPage1.Controls.Add(this.Signal_chart);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.Signal);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.Array_sig);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.Poriadok);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.Array_Otsch);
            this.tabPage1.Controls.Add(this.Text_num_of_sob_vector);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1070, 544);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Вторая часть задания";
            // 
            // Sobstv_fun
            // 
            chartArea13.Name = "ChartArea1";
            this.Sobstv_fun.ChartAreas.Add(chartArea13);
            legend13.Enabled = false;
            legend13.HeaderSeparator = System.Windows.Forms.DataVisualization.Charting.LegendSeparatorStyle.Line;
            legend13.Name = "Legend1";
            this.Sobstv_fun.Legends.Add(legend13);
            this.Sobstv_fun.Location = new System.Drawing.Point(587, 29);
            this.Sobstv_fun.Name = "Sobstv_fun";
            series13.BorderWidth = 2;
            series13.ChartArea = "ChartArea1";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series13.Legend = "Legend1";
            series13.Name = "Series1";
            this.Sobstv_fun.Series.Add(series13);
            this.Sobstv_fun.Size = new System.Drawing.Size(477, 234);
            this.Sobstv_fun.TabIndex = 73;
            this.Sobstv_fun.Text = "chart3";
            // 
            // Sobstv_znach
            // 
            chartArea14.Name = "ChartArea1";
            this.Sobstv_znach.ChartAreas.Add(chartArea14);
            legend14.Enabled = false;
            legend14.Name = "Legend1";
            this.Sobstv_znach.Legends.Add(legend14);
            this.Sobstv_znach.Location = new System.Drawing.Point(6, 296);
            this.Sobstv_znach.Name = "Sobstv_znach";
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series14.Legend = "Legend1";
            series14.Name = "Series1";
            this.Sobstv_znach.Series.Add(series14);
            this.Sobstv_znach.Size = new System.Drawing.Size(477, 242);
            this.Sobstv_znach.TabIndex = 72;
            this.Sobstv_znach.Text = "chart2";
            // 
            // Signal_chart
            // 
            chartArea15.Name = "ChartArea1";
            this.Signal_chart.ChartAreas.Add(chartArea15);
            legend15.Enabled = false;
            legend15.Name = "Legend1";
            this.Signal_chart.Legends.Add(legend15);
            this.Signal_chart.Location = new System.Drawing.Point(6, 18);
            this.Signal_chart.Name = "Signal_chart";
            series15.BorderWidth = 2;
            series15.ChartArea = "ChartArea1";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series15.Legend = "Legend1";
            series15.Name = "Series1";
            this.Signal_chart.Series.Add(series15);
            this.Signal_chart.Size = new System.Drawing.Size(477, 242);
            this.Signal_chart.TabIndex = 71;
            this.Signal_chart.Text = "chart1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.F2);
            this.groupBox2.Controls.Add(this.F1);
            this.groupBox2.Controls.Add(this.P3);
            this.groupBox2.Controls.Add(this.P1);
            this.groupBox2.Controls.Add(this.F3);
            this.groupBox2.Controls.Add(this.P2);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.A1);
            this.groupBox2.Controls.Add(this.A2);
            this.groupBox2.Controls.Add(this.A3);
            this.groupBox2.Location = new System.Drawing.Point(578, 374);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(285, 127);
            this.groupBox2.TabIndex = 70;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры сигнала";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label6.Location = new System.Drawing.Point(25, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 15);
            this.label6.TabIndex = 42;
            this.label6.Text = "Фаза :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label7.Location = new System.Drawing.Point(25, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 15);
            this.label7.TabIndex = 41;
            this.label7.Text = "Частоты :";
            // 
            // F2
            // 
            this.F2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.F2.Location = new System.Drawing.Point(178, 57);
            this.F2.Name = "F2";
            this.F2.Size = new System.Drawing.Size(37, 23);
            this.F2.TabIndex = 37;
            this.F2.Text = "0,015";
            // 
            // F1
            // 
            this.F1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.F1.Location = new System.Drawing.Point(135, 57);
            this.F1.Name = "F1";
            this.F1.Size = new System.Drawing.Size(37, 23);
            this.F1.TabIndex = 35;
            this.F1.Text = "0,02";
            // 
            // P3
            // 
            this.P3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.P3.Location = new System.Drawing.Point(221, 86);
            this.P3.Name = "P3";
            this.P3.Size = new System.Drawing.Size(37, 23);
            this.P3.TabIndex = 40;
            this.P3.Text = "0";
            // 
            // P1
            // 
            this.P1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.P1.Location = new System.Drawing.Point(135, 86);
            this.P1.Name = "P1";
            this.P1.Size = new System.Drawing.Size(37, 23);
            this.P1.TabIndex = 36;
            this.P1.Text = "0,5";
            // 
            // F3
            // 
            this.F3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.F3.Location = new System.Drawing.Point(221, 57);
            this.F3.Name = "F3";
            this.F3.Size = new System.Drawing.Size(37, 23);
            this.F3.TabIndex = 39;
            this.F3.Text = "0,03";
            // 
            // P2
            // 
            this.P2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.P2.Location = new System.Drawing.Point(178, 86);
            this.P2.Name = "P2";
            this.P2.Size = new System.Drawing.Size(37, 23);
            this.P2.TabIndex = 38;
            this.P2.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label5.Location = new System.Drawing.Point(25, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 15);
            this.label5.TabIndex = 33;
            this.label5.Text = "Амплитуды :";
            // 
            // A1
            // 
            this.A1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.A1.Location = new System.Drawing.Point(135, 24);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(37, 23);
            this.A1.TabIndex = 30;
            this.A1.Text = "5";
            // 
            // A2
            // 
            this.A2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.A2.Location = new System.Drawing.Point(178, 24);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(37, 23);
            this.A2.TabIndex = 31;
            this.A2.Text = "8";
            // 
            // A3
            // 
            this.A3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.A3.Location = new System.Drawing.Point(221, 24);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(37, 23);
            this.A3.TabIndex = 32;
            this.A3.Text = "4";
            // 
            // Signal
            // 
            this.Signal.Location = new System.Drawing.Point(606, 503);
            this.Signal.Name = "Signal";
            this.Signal.Size = new System.Drawing.Size(240, 45);
            this.Signal.TabIndex = 69;
            this.Signal.Text = "Нарисовать сигнал";
            this.Signal.UseVisualStyleBackColor = true;
            this.Signal.Click += new System.EventHandler(this.Signal_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label4.Location = new System.Drawing.Point(736, 333);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 15);
            this.label4.TabIndex = 68;
            this.label4.Text = "Кол-во сиг. сост.:";
            // 
            // Array_sig
            // 
            this.Array_sig.Location = new System.Drawing.Point(859, 332);
            this.Array_sig.Name = "Array_sig";
            this.Array_sig.Size = new System.Drawing.Size(54, 20);
            this.Array_sig.TabIndex = 67;
            this.Array_sig.Text = "3";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label15.Location = new System.Drawing.Point(732, 302);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 15);
            this.label15.TabIndex = 66;
            this.label15.Text = "Порядок";
            // 
            // Poriadok
            // 
            this.Poriadok.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.Poriadok.Location = new System.Drawing.Point(859, 302);
            this.Poriadok.Name = "Poriadok";
            this.Poriadok.Size = new System.Drawing.Size(54, 21);
            this.Poriadok.TabIndex = 65;
            this.Poriadok.Text = "60";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label13.Location = new System.Drawing.Point(732, 273);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 15);
            this.label13.TabIndex = 64;
            this.label13.Text = "Кол-во отсчетов:";
            // 
            // Array_Otsch
            // 
            this.Array_Otsch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.Array_Otsch.Location = new System.Drawing.Point(859, 270);
            this.Array_Otsch.Name = "Array_Otsch";
            this.Array_Otsch.Size = new System.Drawing.Size(54, 21);
            this.Array_Otsch.TabIndex = 63;
            this.Array_Otsch.Text = "600";
            // 
            // Text_num_of_sob_vector
            // 
            this.Text_num_of_sob_vector.Location = new System.Drawing.Point(859, 3);
            this.Text_num_of_sob_vector.Name = "Text_num_of_sob_vector";
            this.Text_num_of_sob_vector.Size = new System.Drawing.Size(100, 20);
            this.Text_num_of_sob_vector.TabIndex = 62;
            this.Text_num_of_sob_vector.Text = "0";
            this.Text_num_of_sob_vector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(626, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 15);
            this.label3.TabIndex = 61;
            this.label3.Text = "График собственных функций";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(124, 263);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(223, 15);
            this.label2.TabIndex = 60;
            this.label2.Text = "График собственнных значений";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(191, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 15);
            this.label1.TabIndex = 59;
            this.label1.Text = "График сигнала";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Poligarm_zat);
            this.groupBox1.Controls.Add(this.Gauss);
            this.groupBox1.Controls.Add(this.Poligarm);
            this.groupBox1.Location = new System.Drawing.Point(497, 263);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 105);
            this.groupBox1.TabIndex = 58;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выберите сигнал:";
            // 
            // Poligarm_zat
            // 
            this.Poligarm_zat.AutoSize = true;
            this.Poligarm_zat.Location = new System.Drawing.Point(17, 69);
            this.Poligarm_zat.Name = "Poligarm_zat";
            this.Poligarm_zat.Size = new System.Drawing.Size(194, 17);
            this.Poligarm_zat.TabIndex = 2;
            this.Poligarm_zat.TabStop = true;
            this.Poligarm_zat.Text = "Полигармонический затухающий";
            this.Poligarm_zat.UseVisualStyleBackColor = true;
            this.Poligarm_zat.CheckedChanged += new System.EventHandler(this.Poligarm_zat_CheckedChanged);
            // 
            // Gauss
            // 
            this.Gauss.AutoSize = true;
            this.Gauss.Location = new System.Drawing.Point(17, 46);
            this.Gauss.Name = "Gauss";
            this.Gauss.Size = new System.Drawing.Size(104, 17);
            this.Gauss.TabIndex = 1;
            this.Gauss.TabStop = true;
            this.Gauss.Text = "Гауссов сигнал";
            this.Gauss.UseVisualStyleBackColor = true;
            this.Gauss.CheckedChanged += new System.EventHandler(this.Gauss_CheckedChanged);
            // 
            // Poligarm
            // 
            this.Poligarm.AutoSize = true;
            this.Poligarm.Checked = true;
            this.Poligarm.Location = new System.Drawing.Point(17, 23);
            this.Poligarm.Name = "Poligarm";
            this.Poligarm.Size = new System.Drawing.Size(167, 17);
            this.Poligarm.TabIndex = 0;
            this.Poligarm.TabStop = true;
            this.Poligarm.Text = "Полигармонический сигнал";
            this.Poligarm.UseVisualStyleBackColor = true;
            this.Poligarm.CheckedChanged += new System.EventHandler(this.Poligarm_CheckedChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.CHAST_check);
            this.tabPage2.Controls.Add(this.N_text);
            this.tabPage2.Controls.Add(this.M_text);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.NEVYAZKI_TEXTBOX);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.RESULT_BUTTON);
            this.tabPage2.Controls.Add(this.RESULT);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1070, 544);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Первая часть задания";
            // 
            // CHAST_check
            // 
            this.CHAST_check.AutoSize = true;
            this.CHAST_check.Location = new System.Drawing.Point(895, 26);
            this.CHAST_check.Name = "CHAST_check";
            this.CHAST_check.Size = new System.Drawing.Size(108, 17);
            this.CHAST_check.TabIndex = 59;
            this.CHAST_check.Text = "Частный случай";
            this.CHAST_check.UseVisualStyleBackColor = true;
            // 
            // N_text
            // 
            this.N_text.Location = new System.Drawing.Point(731, 26);
            this.N_text.Name = "N_text";
            this.N_text.Size = new System.Drawing.Size(40, 20);
            this.N_text.TabIndex = 58;
            this.N_text.Text = "4";
            // 
            // M_text
            // 
            this.M_text.Location = new System.Drawing.Point(812, 26);
            this.M_text.Name = "M_text";
            this.M_text.Size = new System.Drawing.Size(35, 20);
            this.M_text.TabIndex = 57;
            this.M_text.Text = "4";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label21.Location = new System.Drawing.Point(785, 27);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(18, 15);
            this.label21.TabIndex = 56;
            this.label21.Text = "M";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label20.Location = new System.Drawing.Point(709, 27);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 15);
            this.label20.TabIndex = 55;
            this.label20.Text = "N";
            // 
            // NEVYAZKI_TEXTBOX
            // 
            this.NEVYAZKI_TEXTBOX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.NEVYAZKI_TEXTBOX.Location = new System.Drawing.Point(884, 63);
            this.NEVYAZKI_TEXTBOX.Name = "NEVYAZKI_TEXTBOX";
            this.NEVYAZKI_TEXTBOX.Size = new System.Drawing.Size(100, 21);
            this.NEVYAZKI_TEXTBOX.TabIndex = 54;
            this.NEVYAZKI_TEXTBOX.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label19.Location = new System.Drawing.Point(739, 64);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 15);
            this.label19.TabIndex = 53;
            this.label19.Text = "Невязки";
            // 
            // RESULT_BUTTON
            // 
            this.RESULT_BUTTON.Location = new System.Drawing.Point(797, 97);
            this.RESULT_BUTTON.Name = "RESULT_BUTTON";
            this.RESULT_BUTTON.Size = new System.Drawing.Size(140, 40);
            this.RESULT_BUTTON.TabIndex = 52;
            this.RESULT_BUTTON.Text = "Результат";
            this.RESULT_BUTTON.UseVisualStyleBackColor = true;
            this.RESULT_BUTTON.Click += new System.EventHandler(this.RESULT_BUTTON_Click);
            // 
            // RESULT
            // 
            this.RESULT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.RESULT.Location = new System.Drawing.Point(68, 4);
            this.RESULT.Multiline = true;
            this.RESULT.Name = "RESULT";
            this.RESULT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RESULT.Size = new System.Drawing.Size(558, 536);
            this.RESULT.TabIndex = 51;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1117, 606);
            this.Controls.Add(this.Signal_Char);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Signal_Click);
            this.Signal_Char.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Sobstv_fun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sobstv_znach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Signal_chart)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl Signal_Char;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataVisualization.Charting.Chart Sobstv_fun;
        private System.Windows.Forms.DataVisualization.Charting.Chart Sobstv_znach;
        private System.Windows.Forms.DataVisualization.Charting.Chart Signal_chart;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox F2;
        private System.Windows.Forms.TextBox F1;
        private System.Windows.Forms.TextBox P3;
        private System.Windows.Forms.TextBox P1;
        private System.Windows.Forms.TextBox F3;
        private System.Windows.Forms.TextBox P2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox A1;
        private System.Windows.Forms.TextBox A2;
        private System.Windows.Forms.TextBox A3;
        private System.Windows.Forms.Button Signal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Array_sig;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Poriadok;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Array_Otsch;
        private System.Windows.Forms.TextBox Text_num_of_sob_vector;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton Poligarm_zat;
        private System.Windows.Forms.RadioButton Gauss;
        private System.Windows.Forms.RadioButton Poligarm;
        private System.Windows.Forms.CheckBox CHAST_check;
        private System.Windows.Forms.TextBox N_text;
        private System.Windows.Forms.TextBox M_text;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox NEVYAZKI_TEXTBOX;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button RESULT_BUTTON;
        private System.Windows.Forms.TextBox RESULT;
    }
}

