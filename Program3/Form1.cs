﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program3
{
    public partial class Form1 : Form
    {
        int arraysize;
        int smesh_akp; 
        int num_of_signal; 
        double[] Mass_Ampl; 
        double[] Mass_Phase; 
        double[] Mass_Freq; 
        double[] Mass_Ampl_Gauss; 
        double[] Mass_Center; 
        double[] Mass_Dispers; 
        double[] Mass_Phase_2; 
        double[] Mass_Freq_2; 
        double[] Mass_Koef;
        int number_of_sobstv_vector;
        int n, m;
        public Form1()
        {
            InitializeComponent();
            Mass_Ampl = new double[3];
            Mass_Phase = new double[3];
            Mass_Freq = new double[3];
            Mass_Ampl_Gauss = new double[3];
            Mass_Center = new double[3];
            Mass_Dispers = new double[3];
            Mass_Phase_2 = new double[3];
            Mass_Freq_2 = new double[3];
            Mass_Koef = new double[3];
        }

        void InitiateModel()
        {
           
            smesh_akp = Convert.ToInt32(Poriadok.Text);
            arraysize = Convert.ToInt32(Array_Otsch.Text);
            num_of_signal = Convert.ToInt32(Array_sig.Text);
            number_of_sobstv_vector = Convert.ToInt32(Text_num_of_sob_vector.Text);
        }



        private void Poligarm_CheckedChanged(object sender, EventArgs e)
        {
            
            if (Poligarm.Checked)
            {
                label5.Text = "Амплитуды:";
                label6.Text = "Фазы:";
                label7.Text = "Частоты:";

                A1.Text = Convert.ToString(5);
                A2.Text = Convert.ToString(8);
                A3.Text = Convert.ToString(4);

                P1.Text = Convert.ToString(5e-1);
                P2.Text = Convert.ToString(0);
                P3.Text = Convert.ToString(0);

                F1.Text = Convert.ToString(2e-2);
                F2.Text = Convert.ToString(15e-3);
                F3.Text = Convert.ToString(3e-2);

            }
        }

        private void Gauss_CheckedChanged(object sender, EventArgs e)
        {
            if (Gauss.Checked)
            {
                label5.Text = "Амплитуды:";
                label6.Text = "Центры куполов:";
                label7.Text = "Дисперсии:";

                A1.Text = Convert.ToString(5);
                A2.Text = Convert.ToString(8);
                A3.Text = Convert.ToString(4);

                P1.Text = Convert.ToString(150);
                P2.Text = Convert.ToString(300);
                P3.Text = Convert.ToString(450);

                F1.Text = Convert.ToString(9);
                F2.Text = Convert.ToString(6);
                F3.Text = Convert.ToString(7);
            }
        }

        private void Poligarm_zat_CheckedChanged(object sender, EventArgs e)
        {
            if (Poligarm_zat.Checked)
            {
                label5.Text = "Частоты:";
                label6.Text = "Фазы:";
                label7.Text = "Коэф.затухания:";

                A1.Text = Convert.ToString(2e-2);
                A2.Text = Convert.ToString(15e-3);
                A3.Text = Convert.ToString(1e-2);

                P1.Text = Convert.ToString(5e-2);
                P2.Text = Convert.ToString(5e-2);
                P3.Text = Convert.ToString(6e-2);

                F1.Text = Convert.ToString(1e-2);
                F2.Text = Convert.ToString(3e-2);
                F3.Text = Convert.ToString(2e-2);

            }
        }




        private int SVD(int m_m, int n_n, double[] a, double[] u, double[] v, double[] sigma)
        {
            double thr = 1e-4f, nul = 1e-16f;
            int n, m, i, j, l, k, iter, inn, ll, kk;
            bool lort;
            double alfa, betta, hamma, eta, t, cos0, sin0, buf, s;
            n = n_n;
            m = m_m;
            for (i = 0; i < n; i++)
            {

                inn = i * n;
                for (j = 0; j < n; j++)
                    if (i == j) v[inn + j] = 1.0;
                    else v[inn + j] = 0.0;
            }
            for (i = 0; i < m; i++)
            {
                inn = i * n;
                for (j = 0; j < n; j++)
                {
                    u[inn + j] = a[inn + j];
                }
            }

            iter = 0;
            while (true)
            {
                lort = false;
                iter++;
                for (l = 0; l < n - 1; l++)
                    for (k = l + 1; k < n; k++)
                    {
                        alfa = 0.0; betta = 0.0; hamma = 0.0;
                        for (i = 0; i < m; i++)
                        {
                            inn = i * n;
                            ll = inn + l;
                            kk = inn + k;
                            alfa += u[ll] * u[ll];
                            betta += u[kk] * u[kk];
                            hamma += u[ll] * u[kk];
                        }

                        if (Math.Sqrt(alfa * betta) < nul) continue;
                        if (Math.Abs(hamma) / Math.Sqrt(alfa * betta) < thr) continue;

                        lort = true;
                        eta = (betta - alfa) / (2f * hamma);
                        t = (double)((eta / Math.Abs(eta)) / (Math.Abs(eta) + Math.Sqrt(1.0 + eta * eta)));
                        cos0 = (double)(1.0 / Math.Sqrt(1.0 + t * t));
                        sin0 = t * cos0;

                        for (i = 0; i < m; i++)
                        {
                            inn = i * n;
                            buf = u[inn + l] * cos0 - u[inn + k] * sin0;
                            u[inn + k] = u[inn + l] * sin0 + u[inn + k] * cos0;
                            u[inn + l] = buf;

                            if (i >= n) continue;
                            buf = v[inn + l] * cos0 - v[inn + k] * sin0;
                            v[inn + k] = v[inn + l] * sin0 + v[inn + k] * cos0;
                            v[inn + l] = buf;
                        }
                    }

                if (!lort) break;
            }

            for (i = 0; i < n; i++)
            {
                s = 0.0;
                for (j = 0; j < m; j++) s += u[j * n + i] * u[j * n + i];
                s = (double)Math.Sqrt(s);
                sigma[i] = s;
                if (s < nul) continue;
                for (j = 0; j < m; j++) u[j * n + i] /= s;
            }
            
            for (i = 0; i < n - 1; i++)
                for (j = i; j < n; j++)
                    if (sigma[i] < sigma[j])
                    {
                        s = sigma[i]; sigma[i] = sigma[j]; sigma[j] = s;
                        for (k = 0; k < m; k++)
                        { s = u[i + k * n]; u[i + k * n] = u[j + k * n]; u[j + k * n] = s; }
                        for (k = 0; k < n; k++)
                        { s = v[i + k * n]; v[i + k * n] = v[j + k * n]; v[j + k * n] = s; }
                    }

            return iter;
        }
        private double Gauss_kup(double num, double step, double[] Amp, double[] Frequency, double[] Phas) 
        {
            double result = 0;
            for (int i = 0; i < num; i++)
            {
                result += Amp[i] * Math.Sin(2 * Math.PI * Frequency[i] * step + Phas[i]);
            }
            return result;
        }
        private double Gauss_finction(double num, double step, double[] Amp, double[] Center, double[] Disp) 
        {
            double result = 0;
            for (int i = 0; i < num; i++)
            {
                result += Amp[i] * Math.Exp(-((step - Center[i]) * (step - Center[i])) / (Disp[i] * Disp[i]));
            }
            return result;
        }
        private double PoligarmonichFunc(double num, double step, double[] Frequency, double[] Phas, double[] Koef)
        {
            double result = 0;
            for (int i = 0; i < num; i++)
            {
                result += Math.Exp(-Koef[i] * step) * Math.Sin(2 * Math.PI * Frequency[i] * step + Phas[i]);
            }
            return result;
        }

        private double[] AKM(int poryadok, double num, double[] graphic)
        {

            double y;
            double[] kor = new double[(poryadok + 1) * (poryadok + 1)];

            for (int i = 0; i < poryadok + 1; i++)
            {
                y = 0;
                for (int j = 0; j < num - i; j++)
                {
                    y += graphic[j] * graphic[i + j];
                }
                kor[i] = y / (num + 1 - i);
            }

            for (int i = 0; i < (poryadok + 1); i++)
            {
                for (int j = 0; j < (poryadok + 1); j++)
                {
                    kor[i * (poryadok + 1) + j] = kor[Math.Abs(j - i)];
                }
            }
            return kor;

        }

        

       

        private void Signal_Click(object sender, EventArgs e)
        {
            InitiateModel();
            Signal_chart.Series[0].Points.Clear();
            Sobstv_znach.Series[0].Points.Clear();
            Sobstv_fun.Series[0].Points.Clear();
            if (Poligarm.Checked)
            {
                label5.Text = "Амплитуды:";
                label6.Text = "Фазы:";
                label7.Text = "Частоты:";


                Mass_Ampl[0] = Convert.ToDouble(A1.Text);
                Mass_Ampl[1] = Convert.ToDouble(A2.Text);
                Mass_Ampl[2] = Convert.ToDouble(A3.Text);

                Mass_Phase[0] = Convert.ToDouble(P1.Text);
                Mass_Phase[1] = Convert.ToDouble(P2.Text);
                Mass_Phase[2] = Convert.ToDouble(P3.Text);

                Mass_Freq[0] = Convert.ToDouble(F1.Text);
                Mass_Freq[1] = Convert.ToDouble(F2.Text);
                Mass_Freq[2] = Convert.ToDouble(F3.Text);

                double[] func_poligarm = new double[arraysize];

                for (int i = 0; i < arraysize; i++)
                {
                    func_poligarm[i] = Gauss_kup(num_of_signal, i, Mass_Ampl, Mass_Freq, Mass_Phase);
                    Signal_chart.Series[0].Points.AddXY(i, func_poligarm[i]);
                }

                double[] U = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                double[] V = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                double[] Sigm = new double[(smesh_akp + 1) * (smesh_akp + 1)];

                double[] AKM_POLIGARM = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                for (int i = 0; i < (smesh_akp + 1) * (smesh_akp + 1); i++)
                {
                    U[i] = 0;
                    V[i] = 0;
                    Sigm[i] = 0;
                    AKM_POLIGARM[i] = 0;
                }
                AKM_POLIGARM = AKM(smesh_akp, arraysize, func_poligarm);


                SVD(smesh_akp + 1, smesh_akp + 1, AKM_POLIGARM, U, V, Sigm);



                for (int i = 0; i < smesh_akp; i++)
                {
                    Sobstv_znach.Series[0].Points.AddXY(i, Sigm[i]);
                }


                double[] V1 = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                for (int i = 0; i < (smesh_akp + 1) * (smesh_akp + 1); i++)
                {
                    V1[i] = 0;
                }
                for (int i = 0; i < smesh_akp + 1; i++)
                {
                    if (number_of_sobstv_vector > (smesh_akp))
                    {
                        MessageBox.Show("Нет такого собственнго вектора");
                        return;
                    }
                    else V1[i] = V[i * (smesh_akp + 1) + number_of_sobstv_vector];


                }
                for (int i = 0; i < smesh_akp; i++)
                {
                    Sobstv_fun.Series[0].Points.AddXY(i, V1[i]);
                }
            }
            if (Gauss.Checked)
            {


                Mass_Ampl_Gauss[0] = Convert.ToDouble(A1.Text);
                Mass_Ampl_Gauss[1] = Convert.ToDouble(A2.Text);
                Mass_Ampl_Gauss[2] = Convert.ToDouble(A3.Text);

                Mass_Center[0] = Convert.ToDouble(P1.Text);
                Mass_Center[1] = Convert.ToDouble(P2.Text);
                Mass_Center[2] = Convert.ToDouble(P3.Text);

                Mass_Dispers[0] = Convert.ToDouble(F1.Text);
                Mass_Dispers[1] = Convert.ToDouble(F2.Text);
                Mass_Dispers[2] = Convert.ToDouble(F3.Text);

                double[] func_gauss = new double[arraysize];

                for (int i = 0; i < arraysize; i++)
                {
                    func_gauss[i] = Gauss_finction(num_of_signal, i, Mass_Ampl_Gauss, Mass_Center, Mass_Dispers);
                    Signal_chart.Series[0].Points.AddXY(i, func_gauss[i]);
                }
                double[] U = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                double[] V = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                double[] Sigm = new double[(smesh_akp + 1) * (smesh_akp + 1)];

                double[] AKM_POLIGARM = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                for (int i = 0; i < (smesh_akp + 1) * (smesh_akp + 1); i++)
                {
                    U[i] = 0;
                    V[i] = 0;
                    Sigm[i] = 0;
                    AKM_POLIGARM[i] = 0;
                }
                AKM_POLIGARM = AKM(smesh_akp, arraysize, func_gauss);


                SVD(smesh_akp + 1, smesh_akp + 1, AKM_POLIGARM, U, V, Sigm);



                for (int i = 0; i < smesh_akp; i++)
                {
                    Sobstv_znach.Series[0].Points.AddXY(i, Sigm[i]);
                }


                double[] V1 = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                for (int i = 0; i < (smesh_akp + 1) * (smesh_akp + 1); i++)
                {
                    V1[i] = 0;
                }
                for (int i = 0; i < smesh_akp + 1; i++)
                {
                    if (number_of_sobstv_vector > (smesh_akp))
                    {
                        MessageBox.Show("Нет такого собственнго вектора");
                        return;
                    }
                    else V1[i] = V[i * (smesh_akp + 1) + number_of_sobstv_vector];


                }
                for (int i = 0; i < smesh_akp; i++)
                {
                    Sobstv_fun.Series[0].Points.AddXY(i, V1[i]);
                }
            }
            if (Poligarm_zat.Checked)
            {
                Mass_Phase_2[0] = Convert.ToDouble(A1.Text);
                Mass_Phase_2[1] = Convert.ToDouble(A2.Text);
                Mass_Phase_2[2] = Convert.ToDouble(A3.Text);

                Mass_Freq_2[0] = Convert.ToDouble(P1.Text);
                Mass_Freq_2[1] = Convert.ToDouble(P2.Text);
                Mass_Freq_2[2] = Convert.ToDouble(P3.Text);

                Mass_Koef[0] = Convert.ToDouble(F1.Text);
                Mass_Koef[1] = Convert.ToDouble(F2.Text);
                Mass_Koef[2] = Convert.ToDouble(F3.Text);

                double[] func_zat = new double[arraysize];
                for (int i = 0; i < arraysize; i++)
                {
                    func_zat[i] = PoligarmonichFunc(num_of_signal, i, Mass_Freq_2, Mass_Phase_2, Mass_Koef);
                    Signal_chart.Series[0].Points.AddXY(i, func_zat[i]);
                }

                double[] U = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                double[] V = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                double[] Sigm = new double[(smesh_akp + 1) * (smesh_akp + 1)];

                double[] AKM_POLIGARM = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                for (int i = 0; i < (smesh_akp + 1) * (smesh_akp + 1); i++)
                {
                    U[i] = 0;
                    V[i] = 0;
                    Sigm[i] = 0;
                    AKM_POLIGARM[i] = 0;
                }
                AKM_POLIGARM = AKM(smesh_akp, arraysize, func_zat);


                SVD(smesh_akp + 1, smesh_akp + 1, AKM_POLIGARM, U, V, Sigm);



                for (int i = 0; i < smesh_akp; i++)
                {
                    Sobstv_znach.Series[0].Points.AddXY(i, Sigm[i]);
                }


                double[] V1 = new double[(smesh_akp + 1) * (smesh_akp + 1)];
                for (int i = 0; i < (smesh_akp + 1) * (smesh_akp + 1); i++)
                {
                    V1[i] = 0;
                }
                for (int i = 0; i < smesh_akp + 1; i++)
                {
                    if (number_of_sobstv_vector > (smesh_akp))
                    {
                        MessageBox.Show("Нет такого собственнго вектора");
                        return;
                    }
                    else V1[i] = V[i * (smesh_akp + 1) + number_of_sobstv_vector];


                }
                for (int i = 0; i < smesh_akp; i++)
                {
                    Sobstv_fun.Series[0].Points.AddXY(i, V1[i]);
                }
            }
        }

        private double[,] Ed_Matrix(int n)
        {
            double[,] A = new double[n, n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    A[i, j] = ((i == j) ? 1 : 0);
            return A;
        }
      
        double[,] R_Matrix(int n, double[,] A, double[,] Ed)
        {
            double[,] R = new double[n, 2 * n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < 2 * n; j++)
                    R[i, j] = ((j < n) ? A[i, j] : Ed[i, j - n]);

            }
            return R;
        }
    

        private double[,] Direct_Gauss(double[,] R, int n, int m)
        {
            double temp = 0;

            for (int i = 0; i < n; i++)
                for (int j = i + 1; j < n; j++)
                {
                    temp = (R[i, j] / R[i, i]);
                    for (int k = i; k < m; k++)
                        R[j, k] -= (R[i, k] * temp);

                }
            return R;
        }

       

  
        double Det(double[,] R, int n)
        {
            double det = 1;
            for (int i = 0; i < n; i++)
                det *= R[i, i];
            return det;
        }

        private void RESULT_BUTTON_Click(object sender, EventArgs e)
        {
            double[,] mass;
            if (CHAST_check.Checked)
            {
                double[] mass_1 = new double[9] { 1.01, 2.01, 3.01, 4.01, 5.01, 6.01, 7.01, 8.01, 9.01 };
                mass = new double[3, 3] { { 1.01, 2.01, 3.01 }, { 4.01, 5.01, 6.01 }, { 7.01, 8.01, 9.01 } };
                int[,] triangle = new int[3, 3];

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        triangle[i, j] = (int)mass[i, j];
                    }
                }
                double[,] rev = new double[3, 3];
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        rev[i, j] = mass[i, j];
                    }
                }


                double[,] Pryamoy = Direct_Gauss(R_Matrix(3, rev, Ed_Matrix(3)), 3, 6);


                N_text.Text = Convert.ToString(3);
                M_text.Text = Convert.ToString(3);

                double[] U = new double[9];
                double[] V = new double[9];
                double[] Sigma = new double[9];
                SVD(3, 3, mass_1, U, V, Sigma);
                double[,] V_matr = new double[3, 3];
                double[,] U_matr_trans = new double[3, 3];
                double[,] U_matr = new double[3, 3];
                double[,] Sigm_matr = new double[3, 3];
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        V_matr[i, j] = V[i * 3 + j];

                    }
                }

                double det = Det(Pryamoy, 3);


                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        U_matr[i, j] = U[i * 3 + j];

                    }

                }
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        U_matr_trans[i, j] = U_matr[j, i];
                    }
                }



                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (i == j)
                        {
                            if (Sigma[i] >= 0.1 * Sigma[0])
                            {
                                Sigm_matr[i, j] = 1 / Sigma[i];
                            }
                            else Sigm_matr[i, j] = 0;
                        }

                        else Sigm_matr[i, j] = 0;
                    }
                }
                double[,] Multiply_1 = new double[3, 3];
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        for (int k = 0; k < 3; k++)
                        {
                            Multiply_1[i, j] += V_matr[i, k] * Sigm_matr[k, j];
                        }
                    }
                }
                double[,] Psevdo_obratnaya = new double[3, 3];
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        for (int k = 0; k < 3; k++)
                        {
                            Psevdo_obratnaya[i, j] += Multiply_1[i, k] * U_matr_trans[k, j];
                        }
                    }
                }
                Random rnd = new Random();
                double[] mass_b = new double[3];

                for (int i = 0; i < 3; i++)
                {
                    mass_b[i] = Convert.ToDouble(rnd.Next(-2000, 2000)) / 2000;
                }

                double[] Sol = new double[3];
                double[] Psev = new double[3 * 3];
                int z = 0;
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        Psev[z] = Psevdo_obratnaya[i, j];
                        z++;
                    }
                }

                for (int i = 0; i < 3; i++)
                {
                    Sol[i] = 0.0;

                    for (int j = 0; j < 3; j++)
                    {
                        Sol[i] += Psev[i * 3 + j] * mass_b[j];
                    }


                }

                
                Double Err = 0.0;
                for (int i = 0; i < 3; i++)
                {
                    Double Mult = 0.0;
                    for (int j = 0; j < 3; j++)
                    {
                        Mult += mass_1[i * 3 + j] * Sol[j];
                    }

                    Err += (Mult - mass_b[i]) * (Mult - mass_b[i]) / 3;
                }

                NEVYAZKI_TEXTBOX.Text = Convert.ToString(String.Format("{0:F4}", Err));

                StringBuilder sw = new StringBuilder();

                sw.AppendLine("Исходный массив:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(mass[i, j] + "   ");
                    }
                    sw.AppendLine("\n");
                }
                sw.AppendLine("\n");

                sw.AppendLine("Детерминант:" + " " + Math.Round(det, 3) + " ");

                sw.AppendLine("\n");
                sw.AppendLine("Матрица правых собственных векторов:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(Math.Round(V_matr[i, j], 3) + "   ");
                    }
                    sw.AppendLine("\n");
                }
                sw.AppendLine("\n");
                sw.AppendLine("Матрица левых собственных векторов:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(Math.Round(U_matr[i, j], 3) + "   ");
                    }
                    sw.AppendLine("\n");
                }
                sw.AppendLine("\n");
                sw.AppendLine("Транспонированная матрица левых собственных векторов:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(Math.Round(U_matr_trans[i, j], 3) + "   ");
                    }
                    sw.AppendLine("\n");
                }
                sw.AppendLine("\n");
                sw.AppendLine("Матрица собственных значений возведенных с степень -1:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(Math.Round(Sigm_matr[i, j], 3) + "   ");
                    }
                    sw.AppendLine("\n" + "   ");
                }
                sw.AppendLine("\n");
                sw.AppendLine("Псевдообратная матрица:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(Math.Round(Psevdo_obratnaya[i, j], 3) + "   ");
                    }
                    sw.AppendLine("\n");
                }
                sw.AppendLine("\n");
                sw.AppendLine("Вектор свободных членов:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {

                    sw.AppendLine(Math.Round(mass_b[i], 4) + "\n");

                }
                sw.AppendLine("\n");
                sw.AppendLine("Вектор неизвестных:" + "\n" + "\n");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {

                    sw.AppendLine(Math.Round(Sol[i], 4) + "\n");

                }
                sw.AppendLine("\n");

                RESULT.TextAlign = HorizontalAlignment.Center;
                RESULT.Text = sw.ToString();


            }
            else
            {

                n = Convert.ToInt32(N_text.Text);
                m = Convert.ToInt32(M_text.Text);

                if (n == m)
                {




                    mass = new double[n, m];
                    Random rnd = new Random();
                    double[] mass_1 = new double[n * m];
                    for (int i = 0; i < n * m; i++)
                    {
                        mass_1[i] = Convert.ToDouble(rnd.Next(-2000, 2000)) / 2000;
                    }

                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            mass[i, j] = mass_1[i * n + j];

                        }

                    }

                    int[,] triangle = new int[n, m];

                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            triangle[i, j] = (int)mass[i, j];
                        }
                    }


                    double[,] rev = new double[n, m];
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            rev[i, j] = mass[i, j];
                        }
                    }



                    double[,] Pryamoy = Direct_Gauss(R_Matrix(n, rev, Ed_Matrix(n)), n, 2 * m);


                    double[] U = new double[n * m];
                    double[] V = new double[n * m];
                    double[] Sigma = new double[n * m];

                    double det = Det(Pryamoy, n);

                    SVD(n, m, mass_1, U, V, Sigma);
                    double[,] V_matr = new double[n, m];
                    double[,] U_matr_trans = new double[n, m];
                    double[,] U_matr = new double[n, m];
                    double[,] Sigm_matr = new double[n, m];
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            V_matr[i, j] = V[i * 3 + j];

                        }
                    }


                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            U_matr[i, j] = U[i * 3 + j];

                        }

                    }
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            U_matr_trans[i, j] = U_matr[j, i];
                        }
                    }



                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            if (i == j)
                            {
                                if (Sigma[i] >= 0.1 * Sigma[0])
                                {
                                    Sigm_matr[i, j] = 1 / Sigma[i];
                                }
                                else Sigm_matr[i, j] = 0;
                            }

                            else Sigm_matr[i, j] = 0;
                        }
                    }
                    double[,] Multiply_1 = new double[n, m];
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            for (int k = 0; k < n; k++)
                            {
                                Multiply_1[i, j] += V_matr[i, k] * Sigm_matr[k, j];
                            }
                        }
                    }
                    double[,] Psevdo_obratnaya = new double[n, m];
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            for (int k = 0; k < n; k++)
                            {
                                Psevdo_obratnaya[i, j] += Multiply_1[i, k] * U_matr_trans[k, j];
                            }
                        }
                    }

                    double[] mass_b = new double[n];

                    for (int i = 0; i < n; i++)
                    {
                        mass_b[i] = Convert.ToDouble(rnd.Next(-2000, 2000)) / 2000;
                    }

                    double[] Sol = new double[n];
                    double[] Psev = new double[n * m];
                    int z = 0;
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            Psev[z] = Psevdo_obratnaya[i, j];
                            z++;
                        }
                    }

                    for (int i = 0; i < n; i++)
                    {
                        Sol[i] = 0.0;

                        for (int j = 0; j < n; j++)
                        {
                            Sol[i] += Psev[i * n + j] * mass_b[j];
                        }


                    }

                    Double Err = 0.0;
                    for (int i = 0; i < n; i++)
                    {
                        Double Mult = 0.0;
                        for (int j = 0; j < n; j++)
                        {
                            Mult += mass_1[i * n + j] * Sol[j];
                        }

                        Err += (Mult - mass_b[i]) * (Mult - mass_b[i]) / n;
                    }

                    NEVYAZKI_TEXTBOX.Text = Convert.ToString(String.Format("{0:F4}", Err));

                    StringBuilder sw = new StringBuilder();

                    sw.AppendLine("Исходный массив:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            sw.Append(Math.Round(mass[i, j], 2) + "   ");
                        }
                        sw.AppendLine("\n");
                    }
                    sw.AppendLine("\n");

                    sw.AppendLine("Детерминант:" + " " + Math.Round(det, 3) + "   ");

                    sw.AppendLine("\n");
                    sw.AppendLine("Матрица правых собственных векторов:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            sw.Append(Math.Round(V_matr[i, j], 3) + "   ");
                        }
                        sw.AppendLine("\n");
                    }
                    sw.AppendLine("\n");
                    sw.AppendLine("Матрица левых собственных векторов:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            sw.Append(Math.Round(U_matr[i, j], 3) + "   ");
                        }
                        sw.AppendLine("\n");
                    }
                    sw.AppendLine("\n");
                    sw.AppendLine("Транспонированная матрица левых собственных векторов:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            sw.Append(Math.Round(U_matr_trans[i, j], 3) + "   ");
                        }
                        sw.AppendLine("\n");
                    }
                    sw.AppendLine("\n");
                    sw.AppendLine("Матрица собственных значений возведенных с степень -1:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            sw.Append(Math.Round(Sigm_matr[i, j], 3) + "   ");
                        }
                        sw.AppendLine("\n");
                    }
                    sw.AppendLine("\n");
                    sw.AppendLine("Псевдообратная матрица:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            sw.Append(Math.Round(Psevdo_obratnaya[i, j], 3) + "   ");
                        }
                        sw.AppendLine("\n");
                    }
                    sw.AppendLine("\n");
                    sw.AppendLine("Вектор свободных членов:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                    {

                        sw.AppendLine(Math.Round(mass_b[i], 4) + "\n");

                    }
                    sw.AppendLine("\n");
                    sw.AppendLine("Вектор неизвестных:" + "\n" + "\n");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                    {

                        sw.AppendLine(Math.Round(Sol[i], 4) + "\n");

                    }
                    sw.AppendLine("\n");

                    RESULT.TextAlign = HorizontalAlignment.Center;
                    RESULT.Text = sw.ToString();
                }
                else
                {
                    MessageBox.Show("Выберите другие значения N и M");
                    return;
                }
            }
        }
    }

}
